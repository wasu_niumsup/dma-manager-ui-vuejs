
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueLodash from 'vue-lodash'

import App from './App'
import router from './router'
import AbView from './components/AbView.vue';
import JsonView from './components/JsonView.vue'
import ButtonHeader from './components/models/ButtonHeader.vue'
import TxtHeader from './components/models/TxtHeader.vue'
import Storyboard from './components/models/Storyboard.vue'
import StoryboardContent from './components/models/StoryboardContent.vue'
import StoryboardMenu from './components/models/StoryboardMenu.vue'
import StoryboardScreen from './components/models/StoryboardScreen.vue'
import StoryboardActionList from './components/models/ActionList.vue'
import CreateOutput from './components/models/CreateOutput.vue'
import ScreenInput from './components/models/ScreenInput.vue'
import ScreenOutput from './components/models/ScreenOutput.vue'
import API_Input from './components/models/API_Input.vue'
import API_Output from './components/models/API_Output.vue'
import Confirmation from './components/models/Confirmation.vue'
import GlobalList from './components/models/GlobalList.vue'
import CustomModal from './components/models/CustomModal.vue'
import CustomScreenInput from './components/models/CustomScreenInput.vue'
import CustomScreenOutput from './components/models/CustomScreenOutput.vue'
import CustomCreateOutput from './components/models/CustomCreateOutput.vue'
import CustomOutput from './components/models/CustomOutput.vue'
import CustomApiInput from './components/models/CustomAPI_Input.vue'
import CustomApiOutput from './components/models/CustomAPI_Output.vue'
import CustomConditionTargetScreen from './components/models/CustomConditionTargetScreen'



import DynamicGrid from "./common/components/DynamicGrid/Main.vue"
import TopMenu from './common/components/Menu/TopMenu.vue'
import LeftMenu from './common/components/Menu/LeftMenu.vue'
import Footer from './common/components/Footer/Footer.vue'
import AlertDialog from './common/components/AlertDialog/Main.vue'
import CollapseComponent from './components/pages/validateStoryboard/CollapseComponent.vue'
import ValidateStoryboard from "./components/pages/validateStoryboard/Main.vue"
import WindowUploadImage from './common/components/WindowConfirm/WindowConfirm.vue'
import WindowImage from './common/components/WindowImage/WindowImage.vue'
import HideActioneDialog from './components/models/CustomeHideAction.vue'

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/scss/themes/index.scss'
import generalHelpers  from './helper/general-helpers.js'

import 'vue-select/dist/vue-select.css';


import VueEasyCm from 'vue-easycm'
import VueSimpleContextMenu from 'vue-simple-context-menu'

import VueSelect from "vue-cool-select";

import vSelect from 'vue-select'



Vue.use(VueSelect, {
  theme: "bootstrap" // "bootstrap" or 'material-design'
});

// import JSONEditor from 'vue2-jsoneditor'
import VuejsDialog from "vuejs-dialog"
import axios from 'axios'
import VueAxios from 'vue-axios'
import TreeView from "vue-json-tree-view"
import Datetime from 'vue-datetime'

import { Settings } from 'luxon'
// You need a specific loader for CSS files
// Datetime.TIME_24_SIMPLE;
Settings.defaultLocale = 'en'

import VueClipboard from 'vue-clipboard2'

import FontAwesomeIcon from '@fortawesome/vue-fontawesome'
import fontawesome from '@fortawesome/fontawesome'
import brands from '@fortawesome/fontawesome-free-brands'
import faSpinner from '@fortawesome/fontawesome-free-solid'
import "./assets/scss/app.scss";

fontawesome.library.add(brands, faSpinner)

import 'beautify-scrollbar/dist/index.css';
import 'v2-table/dist/index.css';
import V2Table from 'v2-table';

import VueCarousel from 'vue-carousel'

import store from "./store/store";

import 'fancybox/dist/css/jquery.fancybox.css';
import 'fancybox/dist/js/jquery.fancybox';

import Autocomplete from 'v-autocomplete'

// You need a specific loader for CSS files like https://github.com/webpack/css-loader
import 'v-autocomplete/dist/v-autocomplete.css'

Vue.use(Autocomplete)

Vue.use(VueEasyCm)

Vue.use(V2Table);
Vue.use(VueLodash, {name: '_'}) // options is optional

Vue.use(VueCarousel)

Vue.use(VueAxios, axios)
Vue.use(generalHelpers);
Vue.use(BootstrapVue);
Vue.use(VuejsDialog);
Vue.use(TreeView);
Vue.use(VueClipboard);
Vue.use(Datetime);

Vue.component('v-select', vSelect)
Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.component('json-view',JsonView);
Vue.component('vue-simple-context-menu', VueSimpleContextMenu)
// Vue.component('json-editor',JSONEditor);
Vue.component('ab-view',AbView);
Vue.component('dma-btnheader',ButtonHeader);
Vue.component('dma-txtheader',TxtHeader);
Vue.component('dma-main',Storyboard);
Vue.component('dma-content',StoryboardContent);
Vue.component('dma-menu',StoryboardMenu);
Vue.component('dma-screen',StoryboardScreen);
Vue.component('dma-actionlist',StoryboardActionList);
Vue.component('dma-createoutput',CreateOutput);
Vue.component('dma-screeninput',ScreenInput);
Vue.component('dma-screenoutput',ScreenOutput);
Vue.component('dma-apiinput',API_Input);
Vue.component('dma-apioutput',API_Output);
Vue.component('dma-confirmation',Confirmation);
Vue.component('dma-globallist',GlobalList);
Vue.component('vdma-collapse',CollapseComponent);
Vue.component('dma-validatestoryboard',ValidateStoryboard);
Vue.component('dma-modal',CustomModal);
Vue.component('dma-topmenu',TopMenu);
Vue.component('dma-leftmenu',LeftMenu);
Vue.component('dma-footer',Footer);
Vue.component('dma-alertdialog',AlertDialog);
Vue.component('dma-customscreeninput',CustomScreenInput);
Vue.component('dma-customscreenoutput',CustomScreenOutput);
Vue.component('dma-customcreateoutput',CustomCreateOutput);
Vue.component('dma-customapi-input',CustomApiInput);
Vue.component('dma-customapi-output',CustomApiOutput);
Vue.component('dma-custom-output',CustomOutput);
Vue.component('dma-dynamic-grid',DynamicGrid);
Vue.component('dma-condition-target-screen',CustomConditionTargetScreen);
Vue.component('dma-window-input-upload-image',WindowUploadImage);
Vue.component('dma-window-image',WindowImage);
Vue.component('dma-widdow-dialog-hideaction',HideActioneDialog)


export const bus = new Vue();
//Vue.prototype.$bus = bus;

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  next()
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
