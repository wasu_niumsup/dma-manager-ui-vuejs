import Vue from 'vue'
import Router from 'vue-router'
import ListStoryboard from '../pages/listStoryboard/Main'
import transactionLog from "../pages/transactionLog/Main"
import transactionLogSG from "../pages/transactionLogSG/Main"
import OldStoryboard from "../components/models/Storyboard"
import RestoreImage from '@/components/ims/RestoreImage'
import ValidateStory from '@/components/pages/validateStoryboard/Main'
import DebuggingStory from '../pages/debugging/Main'
import Application from '../pages/application/Main'
import WinUploadTemplate from '../pages/WinUploadTemplate/WinUploadTemplate.vue'
import WinUploadActionChain from '../pages/WinUploadActionChain/WinUploadActionChain.vue'
import WinUploadActionChainTemplate from '../pages/WinUploadActionChainTemplate/WinUploadActionChainTemplate.vue'
import Screen from '../pages/Screen/Screen.vue'
import SceenGroupDetial from '../pages/SceenGroupDetial/SceenGroupDetial.vue'


import Tests from '../pages/test/Main.vue'

Vue.use(Router)

export default new Router({
  routes: [

    {
      path: '/',
      component: ListStoryboard,
      meta: {
        title: 'Storyboard'
      }
    },
    {
      path: '/storyBoard/:callType/:storyName',
      name: 'storyboard',
      component: OldStoryboard,
      meta: {
        title: 'Storyboard'
      }
    },
    {
      path: '/restoreimage',
      component: RestoreImage,
      meta: {
        title: 'Restore Image'
      }
    },
    {
      path: '/validateStory',
      component: ValidateStory,
      meta: {
        title: 'Validate Story'
      }
    },
    {
      path: '/transactionLog',
      component: transactionLog,
      meta: {
        title: 'Transaction Log'
      }
    },
    {
      path: '/transactionLogSG',
      component: transactionLogSG,
      meta: {
        title: 'Transaction Log'
      }
    },
    {
      path: '/test',
      component: Tests,
      meta: {
        title: 'Screen for test'
      }
    },
    {
      path: '/debug',
      component: DebuggingStory,
      meta:{
        title : 'Debugging Story'
      }
    },
    {
      path: '/Application',
      component: Application,
      meta: {
        title: 'Application'
      }
    },
    {
      path: '/WinUploadTemplate',
      component: WinUploadTemplate,
      meta: {
        title: 'WinUploadTemplate'
      }
    },
    {
      path: '/WinUploadActionChain',
      component: WinUploadActionChain,
      meta: {
        title: 'WinUploadActionChain'
      }
    },
    {
      path: '/WinUploadActionChainTemplate',
      component: WinUploadActionChainTemplate,
      meta: {
        title: 'WinUploadActionChainTemplate'
      }
    },
    {
      path: '/Screen',
      component: Screen,
      meta: {
        title: 'Screen'
      }
    },{
      path: '/SceenGroupDetial',
      component: SceenGroupDetial,
      meta: {
        title: 'SceenGroupDetial'
      }
    }
  ]
})
