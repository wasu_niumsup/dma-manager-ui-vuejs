import REGEX from "./regex/index"

var TAG = "[common/validateUtil/index.js] ";

var dotCount = 0;

function validate(regex, event) {
  if (regex.test(event.key) && !(event.keyCode >= 8 && event.keyCode <= 46)) {
    return true;
  } else {
    event.preventDefault();
    return false;
  }
}

function validateAll(regex, event) {
  console.log("ValidateAll => ",regex.test(event.key));
  if (regex.test(event.key)) {
    return true;
  } else {
    if(event.key == '.' && dotCount == 0){
      dotCount++;
    } else {
      event.preventDefault();
    }
    return false;
  }
}
function validateValueFistNumber( data) {
  console.log("validateValueFistNumber => ",data);
  console.log("validateValueFistNumber => ",REGEX.FIST_NUMBER.test(data));
  if (REGEX.FIST_NUMBER.test(data)) {
    return true;
  } else {
    return false;
  }
}


function validateValue(regex, data) {
  console.log("validateValue => ",regex.test(data));
  if (regex.test(data)) {
    return true;
  } else {
    return false;
  }
}

function validateForm(form, value) {
  let formIsInvalid = false;

  for (var key in form) {
    let vType = form[key].validateType;
    let dataValue = eval("value." + form[key].value);

    let stateAllowBlank = null;
    let stateVType = null;

    if (!form[key].allowBlank) {
      if (_.isEmpty(dataValue)) {
        stateAllowBlank = false;
      } else {
        stateAllowBlank = null;
      }
    } else {
      if (_.isEmpty(dataValue)) {
        vType = undefined;
      }
    }

    if (vType != undefined) {
      if (vType.test(dataValue)) {
        stateVType = null;
      } else {
        stateVType = false;
      }
    } else {
      stateVType = null;
    }

    if (stateAllowBlank != null || stateVType != null) {
      form[key].state = false;
      formIsInvalid = true;
    } else {
      form[key].state = null;
    }
  }

  if (formIsInvalid) {
    return true;
  } else {
    return false;
  }
}

export default {
  validateForm: validateForm,
  validate: validate,
  validateAll: validateAll,
  validateValue: validateValue ,
  validateValueFistNumber : validateValueFistNumber,
  REGEX: REGEX
}
