import {
  bus
} from "../../../main";
import constantUtil from "../../constantUtil/index";
import uiUtil from "../../uiUtil/index";
const Vue = bus;
const TAG = "[/common/apiUtil/callServices/index.js]";

function globalHeader() {
  return {
    'Content-type': 'application/json'
  };
}

function createBodyParams(body) {
  return {
    requestData: body
  };
}
/**
 * Function doGet() Api
 * @param {string} url
 * @param {object} body
 * @param {object} headers
 *
 * @return Promise
 */
function doGet(url, body, headers) {
  let setHeaders = {};
  if (!_.isEmpty(headers)) {
    setHeaders = headers;
  }
  return bus.axios.get(url, createBodyParams(body), setHeaders);
}
/**
 * Function doPost() Api
 * @param {string} url
 * @param {object} body
 * @param {object} headers
 *
 * @return Promise
 */
function doPost(url, body, headers) {
  let setHeaders = {};
  if (!_.isEmpty(headers)) {
    setHeaders = headers;
  }
  return bus.axios.post(url, createBodyParams(body), setHeaders);
}


/**
 * Function doPost() Api
 * @param {string} url
 * @param {object} body
 * @param {object} headers
 *
 * @return Promise
 */
function doPostWithCatch(url, body, headers) {
  try {
    let setHeaders = {};
    if (!_.isEmpty(headers)) {
      setHeaders = headers;
    }
    return bus.axios.post(url, createBodyParams(body), setHeaders);
  } catch (err) {
    uiUtil.error.readError(err);
  }
}

function doPost_DefaultBody(url, body, headers) {
  let setHeaders = {};
  if (!_.isEmpty(headers)) {
    setHeaders = headers;
  }
  return bus.axios.post(url, body, setHeaders);
}
/**
 * Using check status from call services
 * @param {object} response
 */
function checkSuccessStatus(response) {
  console.log(TAG + "[checkSuccessStatus] = ", response.status);
  if (response.status == 200) {
    return true;
  }
  return false;
}
/**
 *
 * @param {Request} request
 * @param {Function} doSuccess
 * @param {Function} doFailed
 * @param {Function} doCatch
 */
function validateResponse(request, doSuccess, doFailed, doCatch) {
  request.then(response => {
      if (response.status == 200) {
        const success = response.data.success;
        if (success) {
          console.log(TAG + "doSuccess", response);
          doSuccess(response);
        } else {
          if (doFailed != undefined && _.isFunction(doFailed)) {
            console.log(TAG + "doFailed", response);
            doFailed(response);
          } else {
            console.log(TAG + "doFailed auto function", response);
            uiUtil.bus.post(constantUtil.EVENT.GLOBALLOADED);
            var message;
            if (_.isEmpty(response.data.responseDesc)) {
              message = "Please contact admin.";
            } else {
              message = response.data.responseDesc;
            }
            uiUtil.bus.post(
              constantUtil.EVENT.SHOW_ALERT_DIALOG,
              message
            );
          }
        }
      } else {

        uiUtil.bus.post(constantUtil.EVENT.GLOBALLOADED);

        uiUtil.bus.post(
          constantUtil.EVENT.SHOW_ALERT_DIALOG,
          response.status + " " + response.statusText
        );

      }
    }).then(() => {
      uiUtil.bus.post(constantUtil.EVENT.GLOBALLOADED);
    })
    .catch(err => {
      if (doCatch != undefined && _.isFunction(doCatch)) {
        doCatch();
      } else {
        uiUtil.bus.post(constantUtil.EVENT.GLOBALLOADED);
        uiUtil.error.readError(err);
      }
    });
}
/**
 * Using for get body data from response
 * @param {object} response
 */
function getResponseData(response) {
  return response.data;
}
export default {
  doGet: doGet,
  doPost: doPost,
  doPostWithCatch: doPostWithCatch,
  checkSuccessStatus: checkSuccessStatus,
  validateResponse: validateResponse,
  getResponseData: getResponseData,
  doPost_DefaultBody: doPost_DefaultBody
}
