import bus from "../bus/index"

var TAG = "[/common/uiUtil/error/index.js]"

function readError(err, extUrl) {
  console.log(TAG + " [error] ", err);
  const baseUrl = window.location.protocol + '//' + window.location.hostname;
  let url = baseUrl + "/dmaManager/process/Login";
  if (extUrl != undefined) {
    url = baseUrl + extUrl;
  }

  const response = err.response;
  console.log(TAG + " Error = ", err);
  if (response == undefined) {
    bus.post("showDefaultAlertDialog", err.message);
  } else {
    if (response.status == 401) {
      bus.post("showDefaultAlertDialog", "Session Expired !!!", "Message", url);
    } else {
      bus.post("showDefaultAlertDialog", response.statusText);
    }
  }
}


export default {
  readError: readError
}
