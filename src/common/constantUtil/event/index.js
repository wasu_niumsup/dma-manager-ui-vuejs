var TAG = "[/common/constantUtil/event/index.js] ";


export default {
  SHOWSCREEN: "showScreen",
  /**
   * DDDDDDDDDDDDDDDDDDDAAAAAAAAAAAAAAAAAA
   */
  SCREENCHANGE: "screenChange",
  SCREENUPDATE: "screenUpdate",
  SCREENCHOOSE: "screenChoose",
  AFTER_SCREENCHOOSE: "afterScreenChoose",
  SHOWACTIONLIST: "showActionList",
  DELETESCREEN: "deleteScreen",
  UPGRADESCREEN: "upgradeScreen",
  SETMENUWHENLOAD: "setMenuWhenLoad",
  DISABLESCREEN: "disableScreen",
  LOADSTORYBOARD: "loadStoryboard",
  ADDSCREEN: "addScreen",
  ADDGROUP: "addGroup",
  AFTERDELETESCREEN: "afterDeleteScreen",
  AFTERDELETESCREEN_FIX: "afterDeleteScreenFix",
  GLOBALLOADING: "globalLoading",
  GLOBALLOADED: "globalLoaded",
  GLOBALLOADING_OPA: "globalLoading_opa",
  GLOBALLOADED_OPA: "globalLoaded_opa",
  GLOBALFORCELOADED: "globalforceLoaded",
  SHOW_GBL_MODAL: 'globalListModal',
  SET_CONFIRM_DIALOG_DATA: 'setConfirmDialogData',
  CALL_API_MODAL: 'callAPIModal',
  CALL_BACK_API_MODAL: 'callBackAPIModal',
  ADD_DATA_FROM_MODAL: 'addDataFromModal',
  SAVE_API_INPUT: 'saveApiInput',
  SAVE_API_OUTPUT: 'saveApiOutput',
  SAVE_CREATE_OUTPUT: 'saveCreateOutput',
  SAVE_CONDITION_TARGET: 'saveConditionTarget',
  CHANGE_API: 'change_Api',
  SAVE_STORYBOARD: 'saveStoryboard',
  LOAD_STORYBOARD_CONTENT: 'loadStoryContent',
  LOAD_STORYBOARD_MENU: 'loadStoryMenu',
  LOAD_GLOBAL_LIST: 'loadGlobalList',
  SAVE_GLOBAL_INPUT_SCREEN: 'saveGlobalInputScreen',
  SAVE_SCREEN_INPUT: 'saveScreenInput',
  SAVE_SCREEN_OUTPUT: 'saveScreenOutput',
  UPGRADE_ALL_SCREEN : 'upgradeAllScreen',
  SAVE_CONFIRMATION: "saveConfirmation",
  SAVE_ALERT_AFTER_CALL_API: "saveAlertAfterCallAPI",
  AFTER_LOAD_STORYBOARD_MENU: "afterLoadStoryContent",
  GET_STORYBOARD_MENU: "getStoryboardMenu",
  OPEN_SCREENINPUT_MODAL: "openScreenInputModal",
  OPEN_SCREENOUTPUT_MODAL: "openScreenOutputModal",
  OPEN_CREATEOUTPUT_MODAL: "openCreateOutputModal",
  OPEN_API_INPUT_MODAL: "openApiInputModal",
  OPEN_API_OUTPUT_MODAL: "openApiOutputModal",
  OPEN_CUSTOM_OUTPUT_MODAL: "openCustomOutput",
  OPEN_CONDITION_TARGET_MODAL: "openConditionTargetModal",
  GET_GLOBALLIST: "getGlobalList",
  GET_STORYBOARD_HEADER: "getStoryboardHeader",
  GET_STORYBOARD_HEADER_AFTER: "getStoryboardHeaderAfter",
  UPDATE_STORYBOARD: "updateStoryboard",
  GET_STORYBOARD_MENU_DATA: "getStoryboardMenuData",
  DELETE_STORYBOARD: 'deleteStoryboard',
  VIEW_JSON_TREE: 'viewJsonTree',
  CONFIRM_DELETE_STORYBOARD: 'confirmDeleteStoryboard',
  DELETE_STORYBOARD_SUCCESS: 'deleteStoryboardSuccess',
  IMPORT_FILE_STORYBOARD: 'importFileStoryboard',
  IMPORT_IMAGE_STORYBOARD: 'importImageStoryboard',
  UPLOAD_IMAGE_ALL_STORYBOARD: 'uploadImageAllStoryboard',
  SET_ITEMS_VALIDATE_IMAGE: 'setItemsValidateImage',
  EXPORT_FILE_STORYBOARD: 'exportFileStoryboard',
  SHOW_ALERT_DIALOG: 'showDefaultAlertDialog',
  SHOW_ALERT_DIALOG_STORY: "showDefaultAlertDialogStory",
  SHOW_VALIDATE_STORYBOARD: "showValidateStoryboard",
  CONFIRM_DELETE_SCREEN: "confirmDeleteScreen",
  DELETE_INPUT: "deleteInput",
  DELETE_OUTPUT: "deleteOutput",
  DELETE_PINLIST: "deletePinList",
  DELETE_TAB_CONFIG: "deleteTabConfig",
  CONFIRM_DELETE_INPUT: "confirmDeleteInput",
  CONFIRM_DELETE_OUTPUT: "confirmDeleteOutput",
  CONFIRM_DELETE_PINLIST: "confirmDeletePinList",
  CONFIRM_DELETE_TAB_CONFIG: "confirmDeleteTabConfig",
  SET_CRITERIA_SEARCH_TRAN_LOG: "setCriteriaSearchTranLog",
  SET_TOOLTIP_IN_GRID_SEARCH_TRAN_LOG: "setTooltipInGridSearchTranLog",
  SET_TOOLTIP_IN_GRID_SEARCH_TRAN_LOG_SG: "setTooltipInGridSearchTranLogSG",
  CALL_FIND_LIST_TRANSACTION_LOG_NEXT_PAGE: "callFindListTransactionLogNextPage",
  VERIFYSCREEN: "verifyScreen_targetNotBlank",
  SEARCH_STORYBOARD: "searchStoryboard",
  VIEW_PUBLISH_APPLICATION: "viewPublishApplication",
  SAVE_APPLICATION: "saveApplication",
  AFTER_IMPORTFILE: "afterImportFile",
  PUBLISH_APPLICATION: "publishApplication",
  SHOW_CONFIG_PARAMETER: "showConfigParameter",
  SAVE_CONFIG_PARAMETER: "saveConfigParameter",
  RESET_INPUT_FILE_IN_RESTORE_IMAGE: "resetInputFileInRestoreImage",
  CHECK_NEED_UPGRADE_STORYBOARD: "needUpgradeStoryboard",
  OPEN_PINLIST_MODAL: "openPinListModal",
  SHOW_WINDOW_CONFIRM: 'showDefaultWindowConfirm',
  SHOW_WINDOW_CONFIRM_GROUP: 'showDefaultWindowConfirmDeleteGroup',
  SHOW_WINDOW_IMAGE: 'showDefaultWindowImage',
  OPEN_WINDOW_ADD_GROUP: "opemWindowAddGroup",
  CONFIM_ADD_WINDOW_ADD_GROUP: "conFirmAddGroupScreen",
  SAVE_ADD_GROUP_SCREEN_IN_STORYBOARD: "saveAddGroupScreenInStoryboard",
  SAVE_REMOVE_GROUP_SCREEN_IN_STORYBOARD: "saveRemoveGroupScreenInStoryboard",
  SAVE_MOVE_GROUP_SCREEN_IN_STORYBOARD: "saveMoveGroupScreenInStoryboard",
  DELETE_SCREEN_GROUP_IN_STORYBOARD: "deleteScreenGroupInStoryboard",
  MOVE_UP_SCREEN_GROUP_IN_STORYBOARD: "moveUpScreenGroupInStoryboard",
  MOVE_DOWN_SCREEN_GROUP_IN_STORYBOARD: "moveDownScreenGroupInStoryboard",
  CHANGE_GROUP_SCREEN: "changeGroupScreen",
  AUTO_FOCUS_CONFIG_ON_CLICK_IMAGE_SCREEN: "autoFocusConfigOnClickImageScreen",
  HIDE_ACTION_DIALOG:"hideActionDialog",
  UPDATE_HIDE_ACTION_SCREEN:"updateHideScreenAction",
  UPDATE_HIDE_ACTION_SCREEN_PAGE_STORYBOARD:"updateHideScreenActionStroryboard",
  SHOW_SEARCH_TARGET: "showSearchTarget",
  CLOSE_SEARCH_TARGET: "closeSearchTarget",
  SET_SCREEN_GROUP:"setListScreenGroup",
  SET_SCREEN_IN_ACTION:"setScreenInAction",
  GET_VALUE_TAB_ICON_CONFIG:'getValueTabIconConfig',
  CHECK_VALIDATION_TAB_CONFIG:'checkValidationTabConfig',
  CHANGE_COLOR_PICKER: "changeColorPicker",
  // CHECK VALIDATE TAB
  //CHECK_TAB_BEFORE_SAVE_STORYBOARD:'checkTabBeforeSaveStoryboard'
}
