const listStory_fields = [{
  label: " ",
  key: "checkList",
  class: "chk"
},
{
  label: " ",
  key: "viewStory",
  class: "chk"
},
{
  label: "Storyboard Name",
  key: "storyName",
  class: "storyName",
  sortable: true
},
{
  label: "Remark",
  key: "remark"
},
{
  label: "Last Updated",
  key: "lastUpdate",
  class: "last"
},
{
  label: "Last Publish",
  key: "lastPublish",
  class: "last"
}
  // ,
  // {
  //   label: "Status",
  //   key: "status",
  //   class: "status"
  // }
  // ,
  // {
  //   label: " ",
  //   key: "ready",
  //   class: "ready p-tb"
  // }
  ,
{
  label: "Delete",
  key: "delete",
  class: "tdDelete"
}
];

const listScreen_fields = [{
  label: " ",
  key: "checkList",
  class: "chk"
},
{
  label: " ",
  key: "viewScreen",
  class: "chk"
},
{
  label: "Storyboard Name",
  key: "storyName",
  class: "storyName",
  sortable: true
},
{
  label: "Remark",
  key: "remark"
}
];

const listApplication_fields = [{
  label: " ",
  key: "view",
  class: "chk"
},
{
  label: "Application Name",
  key: "entityName",
  class: "storyName"
},
{
  label: "Story Name",
  key: "storyName",
  class: "storyName"
},
{
  label: "Last Updated",
  key: "lastUpdate",
  class: "last"
},
{
  label: "Version",
  key: "version",
  class: "status"
},
{
  label: "Last Publish",
  key: "lastPublish",
  class: "last"
},
{
  label: "Status",
  key: "status",
  class: "status"
},
{
  label: "Publish Status",
  key: "publishStatus",
  class: "last"
}
];

const message_error = {
  MSG_ERROR_START_DATE_END_DATE_TRANSACTION_LOG: "Start Time must less than End Time.",
  MSG_ERROR_DUP_GROUP_SCREEN_IN_STORYBOARD_NAME: "Start Time must less than End Time.",
  COMMON: {
    MSG_REQUIRED: 'Please input required field, or the input data is invalid.'
  }
};
// CHECK VALIDATE TAB
//const message_error_tab = "Tabs Configuration not completed"
const message_confirm = {
  MSG_CONFIRM_RELOAD_API_HEADER: "Do you want to reload this API ?",
  MSG_CONFIRM_RELOAD_API: "This Storyboard is from DMA old version.<br/>Do you want to reload this API ?",
  MSG_CONFIRM_DELETE_SCREEN_GROUO_IN_STORYBOARD: "Do you want to delete "
};

export default {
  listStory_fields: listStory_fields,
  listScreen_fields: listScreen_fields,
  listApplication_fields: listApplication_fields,
  isCollapseTransactionServiceGateway: true,
  isCollapseTransactionDMA: true,
  MSG_ERROR: message_error,
  // CHECK VALIDATE TAB
  //MSG_ERROR_TAB: message_error_tab,
  MSG_CONFIRM: message_confirm
}
