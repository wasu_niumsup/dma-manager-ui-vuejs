function covertHeaderColumn(str) {
  var result = "";
  var i = 0;
  var ch = "";
  while (i <= str.length) {
    ch = str.charAt(i);
    if (i == 0) {
      ch = ch.toUpperCase();
    }
    if (ch == ch.toUpperCase()) {
      result += " ";
    }
    result += ch;
    i++;
  }
  return result;
}

export default {
    covertHeaderColumn : covertHeaderColumn
}
