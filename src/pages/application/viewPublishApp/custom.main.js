export default {
  createDropdownListStory(list) {
    let result = [];
    list.forEach(el => {
      const obj = {
        text: el.storyName,
        value: el.storyName
      }
      result.push(obj);
    });
    return result;
  },
  createDropdownListEntityStatus(list) {
    let result = [];
    list.forEach(el => {
      const obj = {
        text: el.statusName,
        value: el.statusName
      }
      result.push(obj);
    });
    return result;
  }
}
