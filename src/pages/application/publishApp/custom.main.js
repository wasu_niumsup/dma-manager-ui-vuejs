export default {
    getNextVersion(c) {
    var result;
    if (c.indexOf('.') > 0) {
      var split = c.split('.');
      var num = parseInt(split[0]);
      var decimal = parseInt(split[1]);
      if (decimal == 99) {
        num += 1;
        decimal = '00';
      } else {
        decimal += 1;
      }
      if (split[1].length == 2 && split[1][0] == '0' && decimal < 10) {
        result = num + ".0" + decimal;
      } else {
        if (decimal == 10 && split[1].length == 1) {
          num += 1;
          decimal = 0;
        }
        result = num + "." + decimal;
      }
    } else {
      result = parseInt(c) + 1;
    }
    return result;
  }
}
