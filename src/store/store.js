import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state : {
    pinList : []
  },
  getters: {
    getPinList(state){
      return state.pinList;
    },
    getDropdownPinList(state){
      return state.pinList.map(item => ({
        text : item.pinName + " : "+item.pinPage,
        value : item.pinPage
      }));
    }
  },
  mutations : {
    setPinList(state , payload){
      console.log("Store setPinList !");
      state.pinList = payload;
    }
  },
  actions : {
    setPinList({ commit }, payload){
      commit('setPinList', payload);
    }
  }
});

export default store;