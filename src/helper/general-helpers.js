import configJson from '../helper/configApi.json'
import SERVICE from './services.js'
import config from '../application.property.json'

export default {
  install: (Vue) => {
    var configApi = configJson;
    var private_value = '';
    var TAG = "[Helpers] => ";
    var info = config;
    config.env = process.env.NODE_ENV;
    config.isDebug = process.env.NODE_ENV == "development";
    console.info(TAG+"info =", info)

    Vue.prototype.$info = {
      get() {
        return info;

      }
    }

    Vue.prototype.$helpers = {
      arrayDifference(arrayA, arrayB) {
        console.log(TAG + "It's work!!");
        private_value = 999;
      },
      debug(obj) {
        console.log(TAG + "Debug It's work!!" + private_value);
      },
      getApi() {
        return configApi;
      },
      getDeserialize(obj) {
        return obj.data;
      },
      getMockupApi(url, params) {
        console.log(TAG + "URL : " + url);
        console.log(TAG + "PARAMS : ", params);
        console.log(TAG + "SERVICE URL === ", SERVICE.URL_LIST_APP_UI, params);
        if (url == 'listAppUi') {
          return this.callApi(SERVICE.URL_LIST_APP_UI, params);
        } else if (url == 'listApi') {
          return this.callApi(SERVICE.URL_LIST_API, params);
        } else if (url == 'listExtraTarget') {
          return this.callApi(SERVICE.URL_LIST_EXTRA_TARGET, params);
        } else if (url == 'listTargetType') {
          return this.callApi(SERVICE.URL_LIST_TARGET_TYPE, params);;
        } else if (url == 'findStoryByName') {
          return this.callApi(SERVICE.URL_FIND_STORY_BY_NAME, params);
        } else if (url == 'upgrade') {
          return this.callApi(SERVICE.URL_UPGRRADE_STORYBOARD, params);
        } else if (url == 'upgradeAll') {
          return this.callApi(SERVICE.URL_UPGRRADE_ALL_STORYBOARD, params);
        } else if (url == 'findServiceInputByName') {
          return this.callApi(SERVICE.URL_LIST_FIND_SERVICE_INPUT_BY_NAME, params);
        } else {
          return {};
        }
      },
      callApi(url, params) {
        if (params === undefined)
          params = {};

        const config = {
          // headers : {
          //     'Content-type': 'application/json'
          // }
        };

        return Vue.axios.post(url, params, config);
      },
      getScreenList(listAppUi) {
        var screenData = [];
        listAppUi.forEach(el => {
          screenData.push({
            text: el.uiName,
            value: el.uiName,
            urlScreenShot: el.urlScreenShot
          });
        });
        return screenData;
      },
      getScreenActiveList(listScreenActive) {
        var screenData = [];
        var i = 0;
        listScreenActive.forEach(el => {
          screenData.push({
            text: el[Object.keys(el)],
            value: i
          });
          i++;
        });
        return screenData;
      },
      getApiList(listApi) {
        var apiList = [];
        var i = 0;
        listApi.forEach(el => {
          apiList.push({
            text: el.apiName,
            value: el.apiName
          });
          i++;
        });
        return apiList;
      },
      getApiObject(listApi) {
        var obj = {};
        var i = 0;
        listApi.forEach(el => {
          obj[el.apiName] = i;
          i++;
        });
        return obj;
      },
      getListExtraTarget(listExtraTarget) {
        var targetList = [];
        listExtraTarget.forEach(el => {
          targetList.push({
            text: el.uiName,
            value: el.uiName
          });
        });
        return targetList;
      },
      getListTargetType(listTargetType) {
        var targetList = [];
        listTargetType.forEach(el => {
          targetList.push({
            text: el.name,
            value: el.api
          });
        });
        return targetList;
      }
    };
  }
};
