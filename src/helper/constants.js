var TAG = "[constants.js] ";

function getDeserialize(response){
    return response.data.responseData;
}

export default {
  EVENT : {
    SHOWSCREEN: "showScreen",
    SCREENCHANGE: "screenChange",
    SCREENCHOOSE: "screenChoose",
    SHOWACTIONLIST: "showActionList",
    DELETESCREEN: "deleteScreen",
    SETMENUWHENLOAD: "setMenuWhenLoad",
    DISABLESCREEN: "disableScreen",
    LOADSTORYBOARD: "loadStoryboard",
    ADDSCREEN: "addScreen",
    AFTERDELETESCREEN: "afterDeleteScreen",
    GLOBALLOADING: "globalLoading",
    GLOBALLOADED: "globalLoaded",
    SHOW_GBL_MODAL: 'globalListModal',
    SET_CONFIRM_DIALOG_DATA: 'setConfirmDialogData',
    CALL_API_MODAL: 'callAPIModal',
    CALL_BACK_API_MODAL: 'callBackAPIModal',
    ADD_DATA_FROM_MODAL: 'addDataFromModal',
    SAVE_API_INPUT: 'saveApiInput',
    SAVE_API_OUTPUT: 'saveApiOutput',
    SAVE_CREATE_OUTPUT: 'saveCreateOutput',
    CHANGE_API: 'change_Api',
    SAVE_STORYBOARD: 'saveStoryboard',
    LOAD_STORYBOARD_CONTENT: 'loadStoryContent',
    LOAD_STORYBOARD_MENU: 'loadStoryMenu',
    LOAD_GLOBAL_LIST: 'loadGlobalList',
    SAVE_SCREEN_INPUT: 'saveScreenInput',
    SAVE_SCREEN_OUTPUT: 'saveScreenOutput',
    SAVE_CONFIRMATION: "saveConfirmation",
    AFTER_LOAD_STORYBOARD_MENU : "afterLoadStoryContent",
    GET_STORYBOARD_MENU : "getStoryboardMenu",
    OPEN_SCREENINPUT_MODAL : "openScreenInputModal",
    OPEN_SCREENOUTPUT_MODAL : "openScreenOutputModal",
    OPEN_CREATEOUTPUT_MODAL : "openCreateOutputModal",
    OPEN_API_INPUT_MODAL : "openApiInputModal",
    OPEN_API_OUTPUT_MODAL : "openApiOutputModal",
    GET_GLOBALLIST : "getGlobalList",
    GET_STORYBOARD_HEADER : "getStoryboardHeader",
    GET_STORYBOARD_HEADER_AFTER : "getStoryboardHeaderAfter",
    UPDATE_STORYBOARD: "updateStoryboard",
    GET_STORYBOARD_MENU_DATA : "getStoryboardMenuData"
  },
  getDeserialize : getDeserialize

}
