# DMA Manager with Vuejs
> A Vue.js project
# Hello
## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

[TOC]

# VueJS Storyboard



##Components Graph

```mermaid
graph LR;
    dma-main-->dma-content;
    dma-main-->dma-txtheader;
    dma-main-->dma-btnheader;
    dma-content-->dma-screen;
    dma-content-->dma-menu;
    dma-content-->dma-modal;
    dma-content-->dma-globallist
    dma-content-->dma-customscreeninput;
    dma-content-->dma-customscreenoutput;
    dma-screen-->dma-actionlist;
    dma-content-->dma-customcreateoutput;
    dma-content-->dma-customapi-input;
    dma-content-->dma-customapi-output;
    dma-content-->dma-confirmation;
    dma-content-->dma-comfirmdelete;
    dma-content-->dma-comfirmdelete-output;
```

##Utilities

### UI - Util 

> Import

```js
import uiUtil from "@common/uiUtil/index";
```

> error => ใช้สำหรับ alert error ออกมา เมื่อ call services failed

- error 
  - readError(error)

  - ```js
    callService
        .then(response => {
        	// do something
    	})
    	.catch(error => {
        	uiUtil.error.readError(error);
    	});
    ```

    ​

> bus => ใช้สำหรับ รับ-ส่งข้อมูลระหว่าง component โดย busKey ต้องตรงกัน ถึงจะเชื่อมกันได้

- bus
  - post(busKey,param1,param2,param...) 

    ```js
    // ใช้ส่งข้อมูลออกไป
    uiUtil.bus.post("bus007",'Hello');
    ```

  - on(busKey, function(param1,param2,param...))

  - ```js
    // รอรับข้อมูล จะเข้า function เมื่อมีการ post ออกมา

    // ES6 pattern
    uiUtil.bus.on("bus007" , msg => {
        console.log(msg);
    });
    // ES5 pattern
    uiUtil.bus.on("bus007" , function(msg) {
        console.log(msg);
    });
    ```

  - off(busKey)

  - > ควรใช้ทุกครั้งก่อนที่จะ on เพื่อป้องกันการเกิด bus ค้าง

    ```js
    // ใช้สำหรับปิด bus event เพื่อไม่ให้เกิดอาการ bus stacked 
    uiUtil.bus.off("bus007");
    ```

    ​

> ตัวอย่างการใช้ bus ... ในตัวอย่างจะใช้ busKey คือ bus001 และ bus002 เพื่อให้เข้าใจง่ายขึ้น

```vue
<script>
import uiUtil from "@common/uiUtil/index";  
export default {
    methods : {
        sendMessage(){
            uiUtil.bus.post("bus001",'Hello Bus 001');
            uiUtil.bus.post("bus002",'Hello Bus 002');
        }  
    },
    created(){
        uiUtil.bus.off("bus001");
    },
    mounted(){
        uiUtil.bus.on("bus001" , msg => {
    		console.log("Your Message is ",msg);
		});
    }
}
</script>
```

> ผลลัพธ์

```
Your Message is Hello Bus 001
```

> จะเห็นได้ว่า ถ้าชื่อตรงกันถึงจะรับส่งค่าระหว่างกันได้เท่านั้น



### API - Util

ใช้สำหรับเรียก Service แบบ get และ post

> Import

```js
import apiUtil from "@common/apiUtil/index";
```

- callService

  - doGet(url, body, headers)

    > Call service ผ่าน method GET

    ```js
    apiUtil.callService.doGet("/apis/callApi",params);
    ```

    ​

  - doPost(url, body, headers)

    > Call service ผ่าน method POST

    ```js
    apiUtil.callService.doPost("/apis/callApi",params);
    ```

    ​

