const storyboard = require('./json/findStoryboard.json');
const upgradeAll = require('./json/upgradeAll.json');
const upgrade = require('./json/upgrade.json');
const listApi = require('./json/listApi.json');
const listAppUi = require('./json/listAppUi.json');
const listTargetType = require('./json/listTargetType.json');
const listExtraTarget = require('./json/listExtraTarget.json');
const findServiceInputByName = require('./json/findServiceInputByName.json');
const listScreenNativeInputType = require('./json/listScreenNativeInputType.json');
const listGlobalValue = require('./json/listGlobalValue.json');
const uploadImgApi = require('./json/upload.json');
const listStory = require('./json/listStory.json');
const uploadRestore = require("./json/uploadRestore.json")
const backupStory = require("./json/backupStory.json")
const findListTransactionLog = require('./json/findListTransactionLog.json');
const listLogSG = require('./json/listLogSG.json');
const listEntity = require('./json/listEntity.json');
const listEntityStatus = require('./json/listEntityStatus.json');
const validateDeleteStory = require('./json/validateDeleteStory.json');
const uploadRestoreImage = require('./json/uploadRestoreImage.json');
const importTemplateStoryboard = require('./json/ImportTemplateStoryboard.json')
const ImportTemplateAllStoryboard = require('./json/ImportTemplateAllStoryboard')
const UploadImageAll = require('./json/UploadImageAll.json')
const restoreStory = require('./json/restoreStory.json')
const appUiByName = require('./json/screen/findAppUiByName.json')
//screen page
const listTemplate = require('./json/screen/listTemplate.json');
const findTemplateByName = require('./json/screen/findTemplateByName.json');

const fileUpload = require('express-fileupload');
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const app = express()
const util = require('util');
const qr = require('./app/qrcode')

const history = [];

console.log(util.format('%s:%s', 'foo', 'dddddddddddddd'));

// var corsOptions = {
//   optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
// }

app.use(cors());
app.use(bodyParser.json({
  limit: '50mb'
}));


app.post('/dmaManager/apis/storyboard/findStoryByName', (req, res, next) => {
  console.log('/loadStoryboard req ' + JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  console.log("[SERVER] DATA = ", storyboard);
  res.statusCode = 200;
  res.send(JSON.stringify(storyboard, null, 3));
});
app.post('/dmaManager/apis/storyboard/upgrade', (req, res, next) => {
  console.log('dmaManager/apis/storyboard/upgrade req ' + JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  console.log("[SERVER] DATA = ", upgrade);
  res.statusCode = 200;
  res.send(JSON.stringify(upgrade, null, 3));
});
app.post('/dmaManager/apis/storyboard/upgradeAll', (req, res, next) => {
  console.log('dmaManager/apis/storyboard/upgradeAll req ' + JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  console.log("[SERVER] DATA = ", upgradeAll);
  res.statusCode = 200;
  res.send(JSON.stringify(upgradeAll, null, 3));
});
app.post('/dmaManager/apis/api/listApi', (req, res, next) => {
  console.log('/listApi req ' + JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(listApi, null, 3));
});
app.post('/dmaManager/apis/ui/listAppUi', (req, res, next) => {
  console.log('/listAppUi req ' + JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(listAppUi, null, 3));
});
app.post('/dmaManager/apis/storyboard/listTargetType', (req, res, next) => {
  console.log('/listTargetType req ' + JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(listTargetType, null, 3));
});
app.post('/dmaManager/apis/storyboard/listExtraTarget', (req, res, next) => {
  console.log('/listExtraTarget req ' + JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(listExtraTarget, null, 3));
});
app.post('/dmaManager/apis/ui/findServiceInputByName', (req, res, next) => {
  console.log('/findServiceInputByName req ' + JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(findServiceInputByName, null, 3));
});
app.post('/dmaManager/apis/storyboard/listScreenNativeInputType', (req, res, next) => {
  console.log('/listScreenNativeInputType req ' + JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(listScreenNativeInputType, null, 3));
});
app.post('/dmaManager/apis/storyboard/listGlobalValue', (req, res, next) => {
  console.log('/listGlobalValue req ' + JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(listGlobalValue, null, 3));
});
app.post('/dmaManager/apis/storyboard/saveStory', (req, res, next) => {
  console.log('/saveStory req ', JSON.stringify(req.body, null, 3));
  storyboard.responseData = req.body.requestData;
  res.statusCode = 200;
  const result = storyboard;
  result.success = true;
  result.status = 'success';
  res.send(JSON.stringify(result, null, 3));
});
app.post('/dmaManager/apis/storyboard/listStory', (req, res, next) => {
  console.log('/saveStory req ', JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(listStory, null, 3));
});
app.post('/dmaManager/apis/storyboard/deleteStoryAndScreen', (req, res, next) => {
  console.log('/saveStory req ', JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  listStory.responseData.storyList.pop();
  res.send(JSON.stringify(listStory, null, 3));
});
app.post('/dmaManager/apis/upload/uploadRestore', (req, res, next) => {
  console.log('/uploadRestore req ', JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(uploadRestore, null, 3));
});
app.post('/dmaManager/apis/upload/uploadRestoreImage', (req, res, next) => {
  console.log('/uploadRestoreImage req ', JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(uploadRestoreImage, null, 3));
});
app.post('/dmaManager/apis/storyboard/backupStory', (req, res, next) => {
  console.log('/backupStory req ', JSON.stringify(req.body, null, 3))
  res.send(backupStory);
});
app.post('/dmaManager/apis/download/downloadBackup', (req, res, next) => {
  console.log('/downloadBackup req ', JSON.stringify(req.body, null, 3))
  res.setHeader("Content-type", "application/dmabk");
  res.setHeader('Content-disposition', 'attachment; filename=dmaBackup.dmabk');
  res.download(__dirname + "/files/dmaBackup.dmabk");
});

app.post('/dmaManager/apis/transactionlog/findListTransactionLog', (req, res, next) => {
  console.log('/saveStory req ', JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(findListTransactionLog, null, 3));
});

app.post('/serviceGateway/apis/json/transactionlog/listLog', (req, res, next) => {
  console.log('/saveStory req ', JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(listLogSG, null, 3));
});

app.post('/dmaManager/apis/entity/listEntity', (req, res, next) => {
  console.log('/listEntity req ', JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(listEntity, null, 3));
});

app.post('/dmaManager/apis/entity/listEntityStatus', (req, res, next) => {
  console.log('/listEntityStatus req ', JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(listEntityStatus, null, 3));
});

app.post('/dmaManager/apis/entity/findEntityByName', (req, res, next) => {
  console.log('/findEntityByName req ', JSON.stringify(req.body, null, 3))
  let entityName = req.body.requestData.entityName;
  const list = listEntity.responseData.entityList;
  let result = {};
  list.forEach(el => {
    if (el.entityName == entityName) {
      result.responseData = el;
      return;
    }
  });
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(result, null, 3));
});

app.post('/dmaManager/apis/entity/saveEntity', (req, res, next) => {
  console.log('/saveEntity req ', JSON.stringify(req.body, null, 3))
  const saveData = req.body.requestData;
  const entityName = req.body.requestData.entityName;
  const list = listEntity.responseData.entityList;
  let result = null;
  list.forEach((el, index, array) => {
    if (el.entityName == entityName) {
      listEntity.responseData.entityList[index] = saveData;
      listEntity.responseData.entityList[index].publishStatus = 'P';
      result = listEntity.responseData.entityList[index];
      return el;
    }
  });
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(result, null, 3);
});

app.post('/dmaManager/apis/storyboard/validateDeleteStoryBoard', (req, res, next) => {
  console.log('/validateDeleteStoryBoard req ', JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  var access = true;
  validateDeleteStory.generateAt = new Date().getTime();
  res.send(JSON.stringify(validateDeleteStory, null, 3));
});

app.post('/dmaManager/apis/ui/listTemplate', (req, res, next) => {
  console.log('/listTemplate req ', JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  var access = true;
  listTemplate.generateAt = new Date().getTime();
  // res.statusCode = 500;
  res.send(JSON.stringify(listTemplate, null, 3));
});

app.post('/dmaManager/apis/ui/findTemplateByName', (req, res, next) => {
  console.log('/findTemplateByName req ', JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  var access = true;
  findTemplateByName.generateAt = new Date().getTime();
  res.send(JSON.stringify(findTemplateByName, null, 3));
});

app.post('/dmaManager/apis/dma5/UploadImageAll', (req, res, next) => {
  console.log('/UploadImageAll req ', JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(UploadImageAll, null, 3));
});


app.post('/dmaManager/apis/storyboard/restoreStory', (req, res, next) => {
  console.log('/restoreStory req ', JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.send(JSON.stringify(restoreStory, null, 3));
});

app.post('/dmaManager/apis/ui/findAppUiByName', (req, res, next) => {
  console.log('/findAppUiByName req ' + JSON.stringify(req.body, null, 3))
  res.setHeader('Content-Type', 'application/json');
  console.log("[SERVER] DATA = ", appUiByName);
  res.statusCode = 200;
  res.send(JSON.stringify(appUiByName, null, 3));
});


// default options
app.use(fileUpload());

app.post('/ims/apis/file/UploadPermanent', function (req, res) {
  if (!req.files)
    return res.status(400).send('No files were uploaded.');

  console.log(req.files);

  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let sampleFile = req.files.files;
  console.log(sampleFile);

  res.send(uploadImgApi);

  //   // Use the mv() method to place the file somewhere on your server
  //   sampleFile.mv('./temp/' + sampleFile.name+ '.jpg', function(err) {
  //     if (err)
  //       return res.status(500).send(err);

  //     res.send('File uploaded!');
  //   });
});

function getTimeNow() {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; //January is 0!
  var yyyy = today.getFullYear();
  var mins = today.getMinutes() < 10 ? '0' + today.getMinutes() : today.getMinutes();
  var times = today.getHours() + ':' + mins + ':' + today.getSeconds();

  if (dd < 10) {
    dd = '0' + dd
  }

  if (mm < 10) {
    mm = '0' + mm
  }
  return mm + '/' + dd + '/' + yyyy + ' ' + times;
}

app.listen(3001, () => console.log('Started server listening on port 3001!'))
